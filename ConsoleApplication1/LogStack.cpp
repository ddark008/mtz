#include "stdafx.h"
#include "LogStack.h"

#include <map>
#include "Stack.h"
using namespace std;

//�� ���� �������������� map ��� int � float �������
map <LogSet, Stack<unsigned int>*> Map4UnInt;
map <LogSet, Stack<int>*> Map4Int;
map <LogSet, Stack<float>*> Map4float;

// ���������� ����� � �����
void LogStack::put(int num, LogSet key)
{
	if (Map4Int.find(key) == Map4Int.end()) {
		Stack<int> *stack = new Stack<int>;
		Map4Int.insert(make_pair(key, stack));
	}
	Map4Int.find(key)->second->push(num);
}

void LogStack::put(unsigned int num, LogSet key)
{
	if (Map4UnInt.find(key) == Map4UnInt.end()) {
		Stack<unsigned int> *stack = new Stack<unsigned int>;
		Map4UnInt.insert(make_pair(key, stack));
	}
	Map4UnInt.find(key)->second->push(num);
}

void LogStack::put(float num, LogSet key)
{
	if (Map4float.find(key) == Map4float.end()) {
		Stack<float> *stack = new Stack<float>;
		Map4float.insert(make_pair(key, stack));
	}
	Map4float.find(key)->second->push(num);
}

Stack<unsigned int> LogStack::getUnIntStack(LogSet key)
{
	if (Map4UnInt.find(key) != Map4UnInt.end()) {
		return *Map4UnInt.find(key)->second;
	}
	else return NULL;
}

Stack<int> LogStack::getIntStack(LogSet key)
{
	if (Map4Int.find(key) != Map4Int.end()) {
		return *Map4Int.find(key)->second;
	}
	else return NULL;
}

Stack<float> LogStack::getfloatStack(LogSet key)
{
	if (Map4float.find(key) != Map4float.end()) {
		return *Map4float.find(key)->second;
	}
	else return NULL;
}