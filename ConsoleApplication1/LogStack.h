#pragma once
#include "Stack.h"


//������������ ������� �����
enum LogSet { Timer_T1, Timer_T, Timer_T_Set, Lokum_I_RMS, Lokum_Activation_MTZ, Lokum_I_Set, Lokum_Current_Ratio,
				Lokum_isTriggered_MTZ};

class LogStack
{
public:
	// ���������� ����� ���� unsigned int � �����
	static void put(unsigned int num, LogSet key);
	// ���������� ����� ���� int � �����
	static void put(int num, LogSet key);
	// ���������� ����� ���� float � �����
	static void put(float num, LogSet key);
	// �������� ����� ����� ��������� �� unsigned int �� ����� key
	static Stack<unsigned int> getUnIntStack(LogSet key);
	// �������� ����� ����� ��������� �� int �� ����� key
	static Stack<int> getIntStack(LogSet key);
	// �������� ����� ����� ��������� �� float �� ����� key
	static Stack<float> getfloatStack(LogSet key);
};
