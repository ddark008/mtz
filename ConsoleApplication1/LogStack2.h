#pragma once

#include <vector> // ���������� ������ ��������
using namespace std;

#pragma pack(push, 1)
struct StructLogs
{
	unsigned int Timer_T1;
	unsigned int Timer_T;
	unsigned int Timer_T_Set;
	int Lokum_I_RMS;
	bool Lokum_Activation_MTZ;
	int Lokum_I_Set;
	float Lokum_Current_Ratio;
	bool Lokum_isTriggered_MTZ;
};
#pragma pack(pop)

class LogStack2
{
public:
	LogStack2();
	~LogStack2();
	//��������� ��� �����
	StructLogs Log;
		// ����� ��������, �������� ������ � ����
	void load();
	//�������� ���� - ������ ��������
	vector<StructLogs> getStack();
	// �������� ��������� �������
	int getIndex();
private:
	//��������� �� ������ �������
	int index;
	//������ ���������� �����
	bool first;
	//
	void zero();
};

