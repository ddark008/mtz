#include "stdafx.h"
#include "Lokum.h"
#include "Timer.h"
#include "Stack.h"
//#include "LogStack.h"

Lokum::Lokum(unsigned int Is, float k, unsigned int T, unsigned int T1, unsigned int CurveType)
{
	//������ ������� ������������
	I_set = Is;
	//���������� ��������
	k_reset_factor = k;
	
	//����� ������������ � ������
	Timer_MTZ.set_T_set(T);
	//����� ������ � ������
	Timer_MTZ.set_T1_set(T1);
	//��� ������
	Timer_MTZ.setCurveType(CurveType);

	//��������� ��������
	I_RMS = 0;
	Block_01 = 0;
	Block_02 = 0;
	Triggering_MTZ = false;
	Activation_MTZ = false;
	Holding_Activation_MTZ = false;
	// ��� ������ ���������� �������� ���� ����� ���������� ���������
	isTriggered_MTZ = false;
}


Lokum::~Lokum()
{
}


bool Lokum::paramsLoad(int I, bool Block_1, bool Block_2)
{
	I_RMS = I;
	//DEBUG
	LS.Log.Lokum_I_RMS = I_RMS;
	Block_01 = Block_1;
	Block_02 = Block_2;
	bool result = process();
	LS.load();
	return result;
}


bool Lokum::process()
{
	float Current_Ratio = (float)I_RMS / (float)I_set;
	//���� ��� ���������, ���������� ���� "������������ ���"
	if (!Holding_Activation_MTZ) {
		Activation_MTZ = false;
	}

	//DEBUG
	LS.Log.Lokum_I_Set = get_I_set();
	LS.Log.Lokum_Current_Ratio = Current_Ratio;

	// ���������, ��� ��� ������ �������, ���������� ���� "���� ���"
	// ���������� �� ������������ ���� ������������ ��� ������ � ��������� ��������� �������
	if ((I_RMS < get_I_set())||((Timer_MTZ.getCurveType() != 0) && (Current_Ratio < 1.2))) {
		//���������� ���������� ���� �����
		isTriggered_MTZ = false;
		
		//���������� �������� � ������
		Timer_MTZ.tic(false, Current_Ratio);
		return false;
	}
	else {
		//������������� �������� ����������� ����� �����
		isTriggered_MTZ = true;
		//������������� �������� �������� ����� �����
		Triggering_MTZ = true;

		//DEBUG
		LS.Log.Lokum_isTriggered_MTZ = isTriggered_MTZ;

		//���������� �������� � ������
		Timer_MTZ.tic(true, Current_Ratio);
	}

	//���� ������ �� �������� ������ ������ �� ���������
	if (!Timer_MTZ.getTimerAlarm()){
		return false;
	}

	//��������� ���� �� ����������
	if (Block_01 || Block_02)
		return false;

	//�������� ����� "������������ ���"
	Activation_MTZ = true;

	//DEBUG
	LS.Log.Lokum_Activation_MTZ = Activation_MTZ;

	return true;
}

int Lokum::get_I_set()
{
	if (isTriggered_MTZ)
		return I_set*k_reset_factor;
	else
		return I_set;
}


void Lokum::set_I_set(int I)
{
	I_set = I;
}


void Lokum::set_k_reset_factor(float k)
{
	k_reset_factor = k;
}

// ��������� �������
void Lokum::set_T_set(unsigned int T)
{
	Timer_MTZ.set_T_set(T);
}


void Lokum::set_T1_set(unsigned int T1)
{
	Timer_MTZ.set_T1_set(T1);
}


void Lokum::setCurveType(unsigned int Type)
{
	Timer_MTZ.setCurveType(Type);
}


void Lokum::set_Is(unsigned int I)
{
	Timer_MTZ.setCurveType(I);
}
