#pragma once
#include "Timer.h"
#include "stack.h"
#include "LogStack2.h"


class Lokum
{
public:
	Lokum(unsigned int Is, float k, unsigned int T, unsigned int T1, unsigned int CurveType);
	~Lokum();
private:
	int I_set;
	int I_RMS;
	int Block_01;
	int Block_02;
	bool Triggering_MTZ;
	bool Activation_MTZ;
	bool Holding_Activation_MTZ;
	Timer Timer_MTZ;
	float k_reset_factor;
	bool isTriggered_MTZ;
	int get_I_set();
	bool process();

public:
	bool paramsLoad(int I, bool Block_01, bool Block_02);
	void set_I_set(int I);
	void set_k_reset_factor(float k);
	// ������ ��������� �������
	void set_T_set(unsigned int T);
	void set_T1_set(unsigned int T1);
	void setCurveType(unsigned int Type);
	void set_Is(unsigned int I);
	//��������� ���������� ���������� ��� �����
};

static LogStack2 LS;
