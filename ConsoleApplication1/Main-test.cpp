// ConsoleApplication1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <ctime>
#include "Lokum.h"
#include "stack.h"
#include "LogStack.h"
#include "LogStack2.h"

using namespace std;

int main(int argc, char* argv[])
{
	// ���� ��� �������� ���������� ����������
	Stack<int> stackInt[4];

	setlocale(LC_ALL, "rus"); // ��������� ������
	cout << "MTZ start\n";
	Lokum MTZ(10, 1, 4, 3, 0); 
	int a[] = { 0,30,0,0,0,0,30,0,0,30,30,0,0,30,0,0,0,0 };
	for (int i = 0; i < sizeof(a)/sizeof(a[0]); i++) {
		stackInt[0].push(a[i]);
		stackInt[1].push(MTZ.paramsLoad(a[i],false, false));
	}

	//�������� ����
	vector<StructLogs> v(LS.getStack());
	int index = LS.getIndex();

	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];                                // ������, � ������� ����� ��������� ������� �����

	time(&rawtime);                               // ������� ���� � ��������
	timeinfo = localtime(&rawtime);               // ������� ��������� �����, �������������� � ���������

	strftime(buffer, 80, "%H:%M:%S,1     %d.%m.%Y", timeinfo); // ����������� ������ �������
	
	// ������ ������ ������ ofstream ��� ������ � ��������� ��� � ������
	ofstream fout("MTZ.csv");
	//����� ���������
	//"t=0  12:14:51,1     10.12.2015" 
	fout << "t=0" << buffer << endl; // ������ ������ � ����
	fout << "0.   time, �.;1.   Lokum_Activation_MTZ;2.   Lokum_Current_Ratio;3.   Lokum_isTriggered_MTZ;4.   Lokum_I_RMS;5.   Lokum_I_Set;" <<
			"6.   Timer_T;7.   Timer_T1;8.   Timer_T_Set" << endl;
	for (int i = index; i < v.size(); i++) {
		fout << i << ";" << v[i].Lokum_Activation_MTZ << ";" << v[i].Lokum_Current_Ratio << ";" << v[i].Lokum_isTriggered_MTZ << ";" << 
		v[i].Lokum_I_RMS << ";" << v[i].Lokum_I_Set << ";" << v[i].Timer_T << ";" << v[i].Timer_T1 << ";" << v[i].Timer_T_Set << endl;
	}
	fout.close(); // ��������� ����

//	for (int i = getIndex(); i < 30; i++) {
//		cout << v[i].Lokum_Current_Ratio << " ";
//	} 

	system("pause");
	return 0;

}

