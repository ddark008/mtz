#ifndef STACK_H
#define STACK_H

#include <cassert> // ��� assert
#include <iostream>

template <typename T>
class Stack
{
private:
	T *stackPtr;	// ��������� �� ����
	const int size;	// ������������ ���������� ��������� � �����
	int top;	// ����� �������� �������� �����
public:
	Stack(int = 1000);	// �� ��������� ������ ����� ����� 100 ���������
	~Stack();	// ����������

	inline void push(const T &);	// ��������� ������� � ������� �����
	inline T pop();	// ������� ������� �� ������� ����� � ������� ���
	inline void printStack();	// ����� ����� �� �����
	inline const T &Peek(int) const;	// n-� ������� �����
	inline int getStackSize() const;	// �������� ������ �����
	inline int getTop() const;	// �������� ����� �������� �������� � �����
};

// ���������� ������� ������� ������ STack

// ����������� �����
template <typename T>
Stack<T>::Stack(int maxSize) :
	size(maxSize) // ������������� ���������
{
	stackPtr = new T[size]; // �������� ������ ��� ����
	top = 0; // �������������� ������� ������� �����;
}

// ������� ����������� �����
template <typename T>
Stack<T>::~Stack()
{
	delete[] stackPtr; // ������� ����
}

// ������� ���������� �������� � ����
template <typename T>
inline void Stack<T>::push(const T &value)
{
	// ��������� ������ �����
	assert(top < size); // ����� �������� �������� ������ ���� ������ ������� �����

	stackPtr[top++] = value; // �������� ������� � ����
}

// ������� �������� �������� �� �����
template <typename T>
inline T Stack<T>::pop()
{
	// ��������� ������ �����
	assert(top > 0); // ����� �������� �������� ������ ���� ������ 0

	return stackPtr[--top]; // ������� ������� �� �����
}

// ������� ���������� n-� ������� �����
template <class T>
inline const T &Stack<T>::Peek(int nom) const
{
	//
	assert(nom <= top);

	return stackPtr[nom]; // ������� n-� ������� �����
}

// ����� ����� �� �����
template <typename T>
inline void Stack<T>::printStack()
{
	for (int ix = 0; ix < top; ix++)
		cout << stackPtr[ix] << " ";
	cout << endl;
}

// ������� ������ �����
template <typename T>
inline int Stack<T>::getStackSize() const
{
	return size;
}

// ������� ����� �������� �������� �����
template <typename T>
inline int Stack<T>::getTop() const
{
	return top;
}


#endif // STACK_H