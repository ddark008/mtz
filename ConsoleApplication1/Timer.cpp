#include "stdafx.h"
#include "Timer.h"
#include <cmath>
#include "Stack.h"
#include "Lokum.h"



Timer::Timer()
{
	//�������� ������
	T = 0;
	// ������ ���������
	T1 = 0;
	// ������� ��������� �������
	T_set = 0;
	// ������� ������� ���������
	T1_set = 0;
	// ��� �������� �������
	CurveType = 0;
	// ���� ������������ ��������� �����
	Timer_alarm = false;
	// ���� ��������� ��������� �������
	T_freeze = false;
	// ���� ��������� ������� ���������
	T1_freeze = true;
	// ������� �������� ����� ���
	Activation_MTZ_now = false;
	// ������� �������� ��������� ���� �� ��������� � �������
	Current_Ratio = 0;
}


Timer::~Timer()
{
}

// �������� ����� ������ �������: �������� ������� �������� ����� ��� � ����  
bool Timer::tic(bool Activation_MTZ, float ratio)
{
	Activation_MTZ_now = Activation_MTZ;
	Current_Ratio = ratio;

	// ����� �������� � ����������� �� ��������� �������� ����� ���
	switch (Activation_MTZ_now)
	{
	case 0: {
		//����������� ������ ���������
		incT1();
		//���� ������ ��������, ���������� �������� ������ � ������������ ������ ���������
		if (T1 >= T1_set) {
			T = 0;
			T1_freeze = true;
		}
		//���� �������� ������ ��������� - ���������� ���
		if (T_freeze == true)
			T = 0;
		T_freeze = false;
		Timer_alarm = false;
		break;
	}
	case 1: {
		incT();
		if (T >=  get_T_set()) {
			Timer_alarm = true;
			T_freeze = true;
		}
		T1 = 0;
		T1_freeze = false;
		break;
	}
	}

	//DEBUG
	LS.Log.Timer_T1 = T1;
	LS.Log.Timer_T = T;
	LS.Log.Timer_T_Set = get_T_set();

	return false;
}


void Timer::incT()
{
	// ��������� ��������� �� ������
	if (!T_freeze)
		T++;
}


void Timer::incT1()
{
	// ��������� ��������� �� ������
	if (!T1_freeze)
		T1++;
}

// ���������� ������� �������
unsigned int Timer::get_T_set()
{
	switch (CurveType)
	{
	//������� ��������� �������� SIT
	case 1: {
		//���������� float ��� ��������������
		float a = 0.14;
		float b = 2.97;
		float c = 0.02;
		return a*T_set / (b*((float)pow(Current_Ratio, c) - 1));
		break;
	}
	//����� ������� ��������� �������� VIT ��� LTI
	case 2: {
		float a = 13.5;
		float b = 1.5;
		return a*T_set / (b*(Current_Ratio - 1));
		break;
	}
	//����������� ������� ��������� �������� EIT
	case 3: {
		float a = 80;
		float b = 0.808;
		float c = 2;
		return a*T_set / (b*((float)pow(Current_Ratio, c) - 1));
		break;
	}
	//������ ������� ��������� �������� UIT
	case 4: {
		float a = 315;
		float c = 2.5;
		return a*T_set / ((float)pow(Current_Ratio, c) - 1);
		break;
	}
	//�������� ���� RI
	case 5: {
		float a = 0.315;
		float b = 0.339;
		float c = 0.236;
		return a*T_set / (b-(float)pow(Current_Ratio, -1)*c);
		break;
	}
	//����������� �������� �������
	default:
		return T_set;
		break;
	}
}

//
void Timer::set_T_set(unsigned int T)
{
	T_set = T;
}

//
void Timer::set_T1_set(unsigned int T1)
{
	T1_set = T1;
}

//
void Timer::setCurveType(unsigned int Type)
{
	CurveType = Type;
}

//
bool Timer::getTimerAlarm()
{
	return Timer_alarm;
}

//
unsigned int Timer::getCurveType()
{
	return CurveType;
}
