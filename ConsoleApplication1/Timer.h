#pragma once
#include "stack.h"

class Timer
{
public:
	Timer();
	~Timer();
private:
	unsigned int T;
	unsigned int T1;
	unsigned int T_set;
	unsigned int T1_set;
	bool Timer_alarm;
	bool T_freeze;
	bool T1_freeze;
	unsigned int CurveType;

public:
	bool tic(bool Activation_MTZ, float ratio);
private:
	bool Activation_MTZ_now;
	void incT();
	void incT1();
	unsigned int get_T_set();
	float Current_Ratio;
public:
	void set_T_set(unsigned int T);
	void set_T1_set(unsigned int T1);
	void setCurveType(unsigned int Type);
	bool getTimerAlarm();
	unsigned int getCurveType();
};

